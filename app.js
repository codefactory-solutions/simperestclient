var app = angular.module('coreApp', ['ngResource', 'ngRoute', 'angularFileUpload']);

app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/home.html'
		})
		.when('/items', {
			templateUrl: 'views/items.html'
		})
		.otherwise({
			redirectTo: '/'
		});
	$httpProvider.interceptors.push('httpBearerInterceptor');
}]);

app.factory('httpBearerInterceptor', function() {
	return {
		request: function(config) {
			var token = '12211211';
			config.headers['Authorization'] = "Bearer " + token;
			return config;
		}
	};
});


app.factory('Item', ['$resource', 'httpBearerInterceptor', function($resource, httpBearerInterceptor) {
	return $resource('http://192.168.1.141:3333/api/items/:id', {
		id: '@id'
	}, {
		update: {
			method: 'PUT' // To send the HTTP Put request when calling this custom update method.
		}
	});
}]);

app.factory('CategoryOptionsService', function($rootScope, $http) {
	var categoryOptionsService = {};
	categoryOptionsService.data = {};
	categoryOptionsService.getOptions = function() {
		$http.get('http://192.168.1.141:3333/api/categories').success(function(data) {
			categoryOptionsService.data.options = data;
		});

		return categoryOptionsService.data;
	};

	return categoryOptionsService;
});


app.controller('myCtrl', ['$scope', 'Item', '$http', 'CategoryOptionsService', 'FileUploader', function($scope, Item, $http, CategoryOptionsService, FileUploader) {
	// contreller as vs scope read this http://codetunnel.io/angularjs-controller-as-or-scope/
	var self = this;

	$scope.categoryOptions = CategoryOptionsService.getOptions();
	// self.selectedCategory = false;

	self.item = new Item();

	self.items = [];

	self.fetchAllItems = function() {
		self.items = Item.query();
	};

	self.createItem = function() {
		self.item.$save(function() {
			self.fetchAllItems();
		});
	};

	self.updateItem = function() {
		self.item.$update(function() {
			self.fetchAllItems();
		});
	};

	self.deleteItem = function(identity) {
		var item = Item.get({
			id: identity
		}, function() {
			item.$delete(function() {
				console.log('Deleting item with id ', identity);
				self.fetchAllItems();
			});
		});
	};

	self.fetchAllItems();

	self.submit = function() {
		if (self.item.id == null) {
			console.log('Saving New Item', self.item);
			self.createItem();
		} else {
			console.log('Upddating Item with id ', self.item.id);
			self.updateItem();
			console.log('Item updated with id ', self.item.id);
		}
		self.reset();
	};

	self.edit = function(id) {
		console.log('id to be edited', id);
		for (var i = 0; i < self.items.length; i++) {
			console.log(self.items[i]._category);
			if (self.items[i].id === id) {
				self.item = angular.copy(self.items[i]);
				break;
			}
		}
	};

	self.remove = function(id) {
		console.log('id to be deleted', id);
		if (self.item.id === id) { //If it is the one shown on screen, reset screen
			self.reset();
		}
		self.deleteItem(id);
	};


	self.reset = function() {
		self.item = new Item();
		$scope.myForm.$setPristine(); //reset Form
	};

	///////////////////////////////////////////

	var uploader = $scope.uploader = new FileUploader({
		url: 'http://192.168.1.141:3333/api/multer',
		alias:"imagefile"
	});

	// FILTERS

	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/ , options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS

	uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
		console.info('onWhenAddingFileFailed', item, filter, options);
	};
	uploader.onAfterAddingFile = function(fileItem) {
		console.info('onAfterAddingFile', fileItem);
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
		console.info('onAfterAddingAll', addedFileItems);
	};
	uploader.onBeforeUploadItem = function(item) {
		console.info('onBeforeUploadItem', item);
	};
	uploader.onProgressItem = function(fileItem, progress) {
		console.info('onProgressItem', fileItem, progress);
	};
	uploader.onProgressAll = function(progress) {
		console.info('onProgressAll', progress);
	};
	uploader.onSuccessItem = function(fileItem, response, status, headers) {
		console.info('onSuccessItem', fileItem, response, status, headers);
	};
	uploader.onErrorItem = function(fileItem, response, status, headers) {
		console.info('onErrorItem', fileItem, response, status, headers);
	};
	uploader.onCancelItem = function(fileItem, response, status, headers) {
		console.info('onCancelItem', fileItem, response, status, headers);
	};
	uploader.onCompleteItem = function(fileItem, response, status, headers) {
		console.info('onCompleteItem', fileItem, response, status, headers);
	};
	uploader.onCompleteAll = function() {
		console.info('onCompleteAll');
	};

	console.info('uploader', uploader);

}]);

app.directive('ngThumb', ['$window', function($window) {
	var helper = {
		support: !!($window.FileReader && $window.CanvasRenderingContext2D),
		isFile: function(item) {
			return angular.isObject(item) && item instanceof $window.File;
		},
		isImage: function(file) {
			var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	};

	return {
		restrict: 'A',
		template: '<canvas/>',
		link: function(scope, element, attributes) {
			if (!helper.support) return;

			var params = scope.$eval(attributes.ngThumb);

			if (!helper.isFile(params.file)) return;
			if (!helper.isImage(params.file)) return;

			var canvas = element.find('canvas');
			var reader = new FileReader();

			reader.onload = onLoadFile;
			reader.readAsDataURL(params.file);

			function onLoadFile(event) {
				var img = new Image();
				img.onload = onLoadImage;
				img.src = event.target.result;
			}

			function onLoadImage() {
				var width = params.width || this.width / this.height * params.height;
				var height = params.height || this.height / this.width * params.width;
				canvas.attr({
					width: width,
					height: height
				});
				canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
			}
		}
	};
}]);